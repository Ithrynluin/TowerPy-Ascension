#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
DOSSIER_COURRANT = os.path.dirname(os.path.abspath(__file__))
DOSSIER_PARENT = os.path.dirname(DOSSIER_COURRANT)
sys.path.append(DOSSIER_PARENT)


def collide_block_top_joueur_bottom(spriteJoueur, spriteBlock):
    if spriteBlock.rect.collidepoint(spriteJoueur.rect.midbottom) :
        #if spriteBlock.rect.top <= spriteJoueur.rect.bottom and spriteBlock.rect.bottom > spriteJoueur.rect.bottom:
        #if (spriteJoueur.rect.left >= spriteBlock.rect.left and spriteJoueur.rect.left < spriteBlock.rect.right) or\
        #(spriteJoueur.rect.right > spriteBlock.rect.left and spriteJoueur.rect.right <= spriteBlock.rect.right):

        spriteJoueur.rect.bottom = spriteBlock.rect.top
        spriteJoueur.aTerre = True
        spriteJoueur.couldWallJumpRight = False
        spriteJoueur.couldWallJumpLeft = False
        return True
    """if (spriteBlock.rect.collidepoint(spriteJoueur.rect.bottomleft) or spriteBlock.rect.collidepoint(spriteJoueur.rect.bottomright)) \
            and spriteJoueur.speed[1] > 0 and not (spriteJoueur.goRight or spriteJoueur.goLeft) \
            and not (spriteBlock.rect.collidepoint(spriteJoueur.rect.midleft) or spriteBlock.rect.collidepoint(spriteJoueur.rect.midright)):
        if not (spriteBlock.rect.topright == spriteJoueur.rect.bottomleft or spriteBlock.rect.topleft == spriteJoueur.rect.bottomright):
            return True"""
    return False

def collide_block_bottom_joueur_top(spriteJoueur, spriteBlock):
    if spriteBlock.rect.collidepoint(spriteJoueur.rect.midtop) :
    #if spriteBlock.rect.bottom >= spriteJoueur.rect.top and spriteBlock.rect.top < spriteJoueur.rect.top:
        #if (spriteJoueur.rect.left >= spriteBlock.rect.left and spriteJoueur.rect.left < spriteBlock.rect.right) or\
                #(spriteJoueur.rect.right > spriteBlock.rect.left and spriteJoueur.rect.right <= spriteBlock.rect.right):
        spriteJoueur.rect.top = spriteBlock.rect.bottom
        spriteJoueur.stopJumping()
        return True
    """if (spriteBlock.rect.collidepoint(spriteJoueur.rect.topleft) or spriteBlock.rect.collidepoint(spriteJoueur.rect.topright)) \
            and spriteJoueur.speed[1] < 0 and not (spriteJoueur.goRight or spriteJoueur.goLeft) \
            and not (spriteBlock.rect.collidepoint(spriteJoueur.rect.midleft) or spriteBlock.rect.collidepoint(spriteJoueur.rect.midright)):
        return True"""
    return False

def collide_block_right_joueur_left(spriteJoueur, spriteBlock):
    if spriteBlock.rect.collidepoint(spriteJoueur.rect.midleft) or spriteJoueur.rect.collidepoint(spriteBlock.rect.midright):
    #if spriteBlock.rect.right >= spriteJoueur.rect.left and spriteBlock.rect.left < spriteJoueur.rect.left:
        #if (spriteJoueur.rect.top >= spriteBlock.rect.top and spriteJoueur.rect.top < spriteBlock.rect.bottom) or\
                #(spriteJoueur.rect.bottom > spriteBlock.rect.top and spriteJoueur.rect.bottom <= spriteBlock.rect.bottom):
        spriteJoueur.rect.left = spriteBlock.rect.right

        if not spriteJoueur.aTerre and spriteJoueur.action[1] == "gauche":
            spriteJoueur.couldWallJumpRight = True

        return True
    return False

def collide_block_left_joueur_right(spriteJoueur, spriteBlock):
    if spriteBlock.rect.collidepoint(spriteJoueur.rect.midright) or spriteJoueur.rect.collidepoint(spriteBlock.rect.midleft):
    #if spriteBlock.rect.left <= spriteJoueur.rect.right and spriteBlock.rect.right > spriteJoueur.rect.right:
        #if (spriteJoueur.rect.top >= spriteBlock.rect.top and spriteJoueur.rect.top < spriteBlock.rect.bottom) or\
                #(spriteJoueur.rect.bottom > spriteBlock.rect.top and spriteJoueur.rect.bottom <= spriteBlock.rect.bottom):
        spriteJoueur.rect.right = spriteBlock.rect.left

        if not spriteJoueur.aTerre and spriteJoueur.action[1] == "droite":
            spriteJoueur.couldWallJumpLeft = True

        return True
    return False


def collide_blocks_joueur(spriteJoueur, groupeBlocks):
    result = False
    aTerre = False
    for block in groupeBlocks:
        if collide_block_top_joueur_bottom(spriteJoueur, block):
            result = True
            aTerre = True
        if collide_block_bottom_joueur_top(spriteJoueur, block):
            result = True
        if collide_block_left_joueur_right(spriteJoueur, block):
            result = True
        if collide_block_right_joueur_left(spriteJoueur, block):
            result = True

    spriteJoueur.aTerre = aTerre
    return result
