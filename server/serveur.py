#!/usr/bin/env python
# coding: utf-8

import  sys, os, random
DOSSIER_COURRANT = os.path.dirname(os.path.abspath(__file__))
DOSSIER_PARENT = os.path.dirname(DOSSIER_COURRANT)
sys.path.append(DOSSIER_PARENT)
import pygame
from channel import ClientChannel
from PodSixNet.Server import Server
from config.config import Config
from client.maps import Map
import collision

class MyServer(Server):
    channelClass = ClientChannel
    DERNIER_ID_JOUEUR = -1
    NB_JOUEUR_MAX = 2

    def __init__(self, *args, **kwargs):
        Server.__init__(self, *args, **kwargs)
        self.clients = []
        self.idMap = 0
        self.carte = None
        self.debutPartie = False
        self.group_joueurs = pygame.sprite.RenderClear()
        print('Server launched')
        print("En attente de %d joueurs" % MyServer.NB_JOUEUR_MAX)

    def Connected(self, channel, addr):
        print('New connection')
        if self.clients.__len__() >= MyServer.NB_JOUEUR_MAX:
            channel.Send({"action":"rejected", "message":"Il y a déjà " + str(MyServer.NB_JOUEUR_MAX) + " joueurs"})
        else:
            self.clients.append(channel)

    def del_client(self, channel):
        print('client deconnecte')
        if self.clients.__contains__(channel):
            self.clients.remove(channel)
            if self.clients.__len__() < MyServer.NB_JOUEUR_MAX:
                self.debutPartie = False
                self.send_all({"action":"stop", "message":"Il semble qu'un joueur a quitté la partie"})

    def get_listeSpritesPlayers(self):
        """Retourne les sprites de tous les joueurs"""
        listSprites = []
        for c in self.clients:
            listSprites.append(c.joueur)
        return  listSprites

    def is_all_channel_ready(self):
        """Retourne vraie si tous les client sont reedy"""
        for c in self.clients:
            if not c.ready:
                return False
        return True

    def choisir_maps(self):
        list_map_propose = []
        for c in self.clients:
            if c.proposeMap != None:
                if int(c.proposeMap) > 0 and int(c.proposeMap) <= Map.get_nb_maps():
                    list_map_propose.append(c.proposeMap)

        if list_map_propose.__len__() == 1:
            return list_map_propose[0]


        if list_map_propose.__len__() > 1:
            index = random.randint(0, (list_map_propose.__len__()-1))
            return list_map_propose[index]

        return random.randint(1, Map.get_nb_maps())


    def get_new_id(self):
        """Retourne un id libre"""
        MyServer.DERNIER_ID_JOUEUR += 1
        return  MyServer.DERNIER_ID_JOUEUR

    def send_all(self, message):
        """Envoie un message à tous les joueurs"""
        for c in self.clients:
            c.Send(message)

        print message

    def update(self):
        """Met à jours tous les clients, ainsi que les autres sprites"""
        tabUpdate = []
        for c in self.clients:
            if c.update():
                tabUpdate.append(c.joueur)
        return tabUpdate

    def updateCollision(self, list_new_pos):
        listPlayersUpdate = {}
        for p in list_new_pos:
            # Test collision
            collision.collide_blocks_joueur(p, self.carte)

            # Test collision por attack corp à corp
            if p.couldAttack > 0:
                joueur_touch = pygame.sprite.spritecollide(p, self.group_joueurs, False)
                print "Touché " + str(joueur_touch)
                # On cherche qui a été touché
                for t in joueur_touch:
                    if t != p:
                        if p.joueurToucher == None:
                            print t.dead()
                            if t.vie >= 0:
                                t.reSpawn()
                            else:
                                t.kill()
                                self.send_all({"action": "killPlayer", "idPlayer": t.id})
                            listPlayersUpdate[t] = {"id": t.id, "action": t.action,
                                                    "rect": (t.rect.centerx, t.rect.centery), "vie": t.vie}
                            p.joueurToucher = t

            listPlayersUpdate[p] = {"id": p.id, "action": p.action, "rect": (p.rect.centerx, p.rect.centery),
                                    "vie": p.vie}

        return listPlayersUpdate

    def updateShots(self, listPlayersUpdate):
        listRectShots = []
        aucunShot = True
        for p in self.group_joueurs.sprites():
            if p.group_shot.__len__() > 0:
                aucunShot = False
            # Collision Shoot et autres joueur
            listCollideJoueur = pygame.sprite.groupcollide(p.group_shot, self.group_joueurs, False, False,
                                                           pygame.sprite.collide_circle_ratio(1))
            if listCollideJoueur.__len__() > 0:
                for shot in listCollideJoueur:
                    joueur = listCollideJoueur[shot][0]
                    # Si le joueur touché n'est pas celui qui tire
                    if joueur != p:
                        print joueur.dead()
                        if joueur.vie >= 0:
                            joueur.reSpawn()
                        else:
                            joueur.kill()
                            self.send_all({"action": "killPlayer", "idPlayer": joueur.id})
                        listPlayersUpdate[joueur] = {"id": joueur.id, "action": joueur.action,
                                                     "rect": (joueur.rect.centerx, joueur.rect.centery),
                                                     "vie": joueur.vie}
                        shot.kill()

            # Collision pour les shoot et les blocks
            pygame.sprite.groupcollide(p.group_shot, self.carte, True, False, pygame.sprite.collide_circle_ratio(1))
            for s in p.group_shot:
                listRectShots.append({'rect': (s.rect.centerx, s.rect.centery), 'direction': s.direction})
        if listRectShots.__len__() > 0 or not aucunShot:
            self.send_all({"action": "shot", "shots": listRectShots})

        return listPlayersUpdate

    def enPartie(self):
        screen = Config.init_screen()
        clock = pygame.time.Clock()
        font = pygame.font.Font(None, 30)

        print self.idMap

        while self.debutPartie:
            # Liste des joueurs dont la position à été mis à joueur
            listPlayersUpdate = {}

            self.Pump()
            clock.tick(60)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return False # closing the window exits the program

            # update des client
            list_new_pos = self.update()

            listPlayersUpdate = self.updateCollision(list_new_pos)

            # Mise à jour des positions des tirs
            listPlayersUpdate = self.updateShots(listPlayersUpdate)# envoie de toutes les données

            if listPlayersUpdate.__len__() != 0:
                self.send_all({"action":"players", "players":listPlayersUpdate.values()})

            # affichage sur le serveur
            screen.fill((0,0,0))
            text = "Il y a actuellement " + str(self.clients.__len__()) + " joueur"
            if self.clients.__len__() > 1:
                text = str(text) + "s"

            # envoie l'id du gagnant et détermine la fin de la partie
            if self.group_joueurs.sprites().__len__() == 1:
                self.send_all({"action": "finPartie", "idGagnant": self.group_joueurs.sprites()[0].id})
                self.debutPartie = False
                return True

            Config.print_text(font,text, screen, (210,255,200))
            pygame.display.flip()

        return True

    def enAttente(self):
        screen = Config.init_screen(360,150)
        clock = pygame.time.Clock()
        font = pygame.font.Font(None, 30)

        # si il y a déjà des client qui attent on les reinitialise
        for c in self.clients:
            c.reinitPlayer()
        self.group_joueurs = pygame.sprite.RenderClear()


        while not self.debutPartie:
            self.Pump()
            clock.tick(60)

            # Event quit
            for event in pygame.event.get():
               if event.type == pygame.QUIT:
                    return False # closing the window exits the program

            # Si tous les joueurs sont là
            if self.clients.__len__() == MyServer.NB_JOUEUR_MAX and not self.debutPartie and self.is_all_channel_ready(): #est-ce-que tous le monde est là !!!
                print "Debut de la partie"

                # Choix Map
                self.idMap = self.choisir_maps()
                self.carte = Map(self.idMap)

                # initialisation de la liste des joueurs
                listSapwn = self.carte.pos_joueurs()
                listPlayers= {}
                for p in self.get_listeSpritesPlayers():

                    p.setSpawn(listSapwn[0])
                    listSapwn.remove(listSapwn[0])
                    listPlayers[p] = {"id":p.id, "rect":(p.rect.centerx, p.rect.centery), "vie":p.vie}
                    self.group_joueurs.add(p)

                # Envoie du lancement de la partie
                self.send_all({"action":"debut", "message": "La partie peut commencer", "map": str(self.idMap), "players":listPlayers.values()})
                self.debutPartie = True



            # Affichage sur le serveur
            screen.fill((0,0,0))
            text = "Il y a actuellement " + str(self.clients.__len__()) + " joueur"
            if self.clients.__len__() > 1:
                text = text + "s"

            Config.print_text(font,text, screen, (210,255,200))
            pygame.display.flip()

        return True


    def main_loop(self):
        encore = True
        while encore:
            self.debutPartie = False
            encore = self.enAttente()
            if encore:
                encore = self.enPartie()



def main_prog():
    try:
        if int(sys.argv[3]) > 0:
            MyServer.NB_JOUEUR_MAX = int(sys.argv[3])
    except IndexError:
        MyServer.NB_JOUEUR_MAX = 2
    try:
        address = sys.argv[1]
        port = sys.argv[2]
        my_server = MyServer(localaddr=(address, int(port)))
        my_server.main_loop()
    except IndexError as e:
        print "Using server.py address port [nb_joueur = 2]"


if __name__ == '__main__':
    main_prog()
    sys.exit(0)
