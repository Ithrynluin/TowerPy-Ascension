# -*- coding: utf-8 -*-
__author__ = 'clement'


import os, sys
import pygame

class Config:
    """Dimension de l'écran"""
    SCREEN_WIDTH= 600
    SCREEN_HEIGHT = 600
    LIST_IMAGE = {}

    """Fonction permettant de charger une image"""
    @staticmethod
    def load_png(name):
        """Load image and return image object"""
        fullname=os.path.join('.',name)
        if Config.LIST_IMAGE.has_key(fullname):
            image = Config.LIST_IMAGE[fullname]
        else:
            try:
                image=pygame.image.load(fullname)
                if image.get_alpha is None:
                    image=image.convert()
                else:
                    image=image.convert_alpha()
                Config.LIST_IMAGE[fullname] = image
            except pygame.error, message:
                print 'Cannot load image:', fullname
                raise SystemExit, message
        return image,image.get_rect()

    @staticmethod
    def init_screen(width = SCREEN_WIDTH, height = SCREEN_HEIGHT):
        """Permet d'init pygame et retourne l'ecran"""
        if not pygame.display.get_init():
            pygame.init()
            screen = pygame.display.set_mode((width, height), pygame.DOUBLEBUF | pygame.HWSURFACE)
        else:
            screen = pygame.display.get_surface()

        if not pygame.font.get_init():
            pygame.font.init()

        if not pygame.mixer.get_init():
            pygame.mixer.init()

        pygame.display.set_caption("TowerPy Ascension")
        pygame.key.set_repeat(1,1)
        return screen

    @staticmethod
    def print_text(font, text, screen, color_text = (255,255,255), color_background = (0,0,0), x = None, y= None, transparence=False):
        """Affcihe un texte sur screen"""
        """x position x où afficher le texte, si None au milieu"""
        """y position y où afficher le texte, si None au milieu"""
        textEncode = unicode(text, 'utf-8')
        text_render = font.render(textEncode, True, color_text, color_background)
        if(x==None):
            x = screen.get_width() / 2 - text_render.get_width() / 2
        if(y==None):
            y = screen.get_height() / 2 - text_render.get_height() / 2

        if transparence:
            text_render.convert_alpha()

        screen.blit(text_render, (x,y))