
class Animation:
    """Classe qui anime un sprite"""
    def __init__(self, interval):
        self.dictImage = {}
        self.interval = interval
        self.derniereAction = ""

    def addImage(self, action, image):
        if not self.dictImage.has_key(action):
            self.dictImage[action] = {"images":[], "pointeur":-1, "couldDone" : self.interval}

        self.dictImage[action]["images"].append(image)

    def getImage(self, action):
        # print action
        if self.derniereAction != "" and self.derniereAction != action:
            #On reinitialise l'action qui n'est plus utilise
            self.dictImage[self.derniereAction]["couldDone"] = self.interval
            self.dictImage[self.derniereAction]["pointeur"] = -1

        if self.dictImage.has_key(action):
            couldDone = self.dictImage[action]["couldDone"]
            pointeur = self.dictImage[action]["pointeur"]
            if couldDone >= self.interval:
                couldDone = 0
                pointeur += 1
                if pointeur >= self.dictImage[action]["images"].__len__():
                    pointeur = 0
            else:
                couldDone += 1

            self.dictImage[action]["couldDone"] = couldDone
            self.dictImage[action]["pointeur"] = pointeur
            image = self.dictImage[action]["images"][pointeur]
            self.derniereAction = action
            return image
