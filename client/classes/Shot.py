#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
DOSSIER_COURRANT = os.path.dirname(os.path.abspath(__file__))
DOSSIER_PARENT = os.path.dirname(os.path.dirname(DOSSIER_COURRANT))
sys.path.append(DOSSIER_PARENT)
import pygame
from pygame.locals import *
from PodSixNet.Connection import ConnectionListener
from config.config import Config

class Shot(pygame.sprite.Sprite, ConnectionListener):
    def __init__(self, x, y, direction):
        pygame.sprite.Sprite.__init__(self)
        if direction == "gauche":
            self.image, self.rect = Config.load_png(os.path.join(DOSSIER_PARENT, 'sprites/fleche_gauche.png'))
        else:
            self.image, self.rect = Config.load_png(os.path.join(DOSSIER_PARENT, 'sprites/fleche_droite.png'))
        self.rect.centerx = x
        self.rect.centery = y

class GroupShots(pygame.sprite.RenderClear, ConnectionListener):
    def __init__(self):
        pygame.sprite.RenderClear.__init__(self)

    def init(self, data):
        """Recupère les tirs"""
        self.empty()
        list = data["shots"]
        for k in list:
            self.add(Shot(k['rect'][0],k['rect'][1], k['direction']))
        print data["shots"]

    def update_shots(self, data):
        """Recupère les tirs"""
        list = data["shots"]
        sprites = self.sprites()
        for sprite in sprites:
            for shot in list:
                sprite.rect.centerx = shot["rect"][0]
                sprite.rect.centery = shot["rect"][1]
                    # if shot["action"] != None:
                    #     sprite.action = shot["action"]

    def Network_shot(self, data):
        #"""Recupère les joueurs"""
        # if self.sprites().__len__() == 0: #Pour le premier envoie
        #     #On créer des sprites représentant les tirs
        #     self.init(data)
        # else: #sinon, il suffit de mettre à jour les postions des sprites existants
        #     self.update_shots(data)
        self.empty()
        self.init(data)
        #On update les animations
        # sprites = self.sprites()
        # for sprite in sprites:
        #     sprite.updateImage()

    def update(self):
        self.Pump()
