#!/usr/bin/env python
# coding: utf-8
__author__ = 'clement'
import os, sys
import time
DOSSIER_COURRANT = os.path.dirname(os.path.abspath(__file__))
DOSSIER_PARENT = os.path.dirname(DOSSIER_COURRANT)
sys.path.append(DOSSIER_PARENT)
import pygame
from pygame.locals import *
from PodSixNet.Connection import connection, ConnectionListener
from config.config import Config
from classes.joueur import GroupJoueurs
from classes.Shot import GroupShots
from maps import Map

class Client(ConnectionListener):
    def __init__(self, host, port, propose_map = None):
        self.Connect((host, port))
        self.run = False
        self.map = 0
        self.id = 0
        self.propose_map = propose_map
        self.text = None
        self.joueurs_group = GroupJoueurs()
        self.shots = GroupShots()

    #def Network(self, data):
    #    print('message de type %s recu ' % data['action'])
    #    print str(data)

    ### Network event/message callbacks ###
    def Network_connected(self, data):
        print('connecte au serveur !')

    def Network_error(self, data):
        print 'error:', data['error'][1]
        self.text = data['error'][1]
        connection.Close()

    def Network_disconnected(self, data):
        print 'Server disconnected'
        # self.text = "Le serveur c'est deconnecte"

    def Network_rejected(self, data):
        print "Rejected : " + data["message"]
        self.text = data["message"]
        connection.Close()

    def Network_debut(self, data):
        print "Debut : " + data["message"] + " map : " + data["map"]
        self.map = data["map"]
        self.run = True
        self.joueurs_group.init(data)

    def Network_id(self, data):
        self.id = data["id"]

    def Network_stop(self, data):
        print data["message"]
        self.run = False
        self.joueurs_group.empty()

    def Network_finPartie(self, data):
        self.run = False
        if int(data["idGagnant"]) == self.id:
            self.text = "Gagné !!!"

    def partie(self):
        """Commence une partie"""
        print "La partie commence"
        # Initialization
        screen = Config.init_screen()
        clock = pygame.time.Clock()
        font = pygame.font.Font(None, 40)
        font_vie = pygame.font.Font(None, 25)
        self.text = None

        # Objects creation
        background_image, background_rect = Config.load_png(os.path.join(DOSSIER_PARENT, 'sprites/background.png'))
        carte = Map(self.map)
        pygame.mixer.music.load(os.path.join(DOSSIER_PARENT, "sound/music.ogg"))
        pygame.mixer.music.play(-1)

        # MAIN LOOP
        while self.run:
            clock.tick(60) # max speed is 60 frames per second
            connection.Pump()
            self.Pump()

            # Events handling
            for event in pygame.event.get():
               if event.type == pygame.QUIT:
                    return False # closing the window exits the program


            touches=pygame.key.get_pressed()
            if(touches[K_q]):
                return False # exit the program
            connection.Send({"action": "touches", "touches":touches})

            self.shots.update()
            self.joueurs_group.update()

            screen.blit(background_image, background_rect)

            carte.draw(screen)
            self.shots.draw(screen)
            self.joueurs_group.draw(screen)
            if self.text != None:
                Config.print_text(font, self.text, screen, (210,255,200))

            # Afficher la vie
            joueur = self.joueurs_group.getJoueurById(self.id)
            if joueur != None:
                vie = joueur.vie
                Config.print_text(font_vie, "Vie : " + str(vie), screen, (0,0,0), (255,255,255,0.2), 0, 0, True)
                if vie < 0:
                    self.text = "Perdu :-("

            pygame.display.flip()

        encore = self.finPartie()
        return encore

    def finPartie(self):
        """Le client attent que la partie commence"""
        # Initialization
        screen = Config.init_screen()
        clock = pygame.time.Clock()
        font = pygame.font.Font(None, 40)
        background_image, background_rect = Config.load_png(os.path.join(DOSSIER_PARENT, 'sprites/background.png'))
        nbAttent = 120

        self.joueurs_group.empty()
        self.shots.empty()

        # MAIN LOOP
        while nbAttent > 0:
            clock.tick(60) # max speed is 60 frames per second
            connection.Pump()
            self.Pump()
            # Events handling
            for event in pygame.event.get():
               if event.type == pygame.QUIT:
                    return False # closing the window exits the program

            touches=pygame.key.get_pressed()
            if(touches[K_q]):
                return False # exit the program

            screen.blit(background_image, background_rect)
            if self.text != None:
                Config.print_text(font, self.text, screen, (210,255,200))
            Config.print_text(font, "Attente de partie dans 2 secondes", screen, (210,255,200), (0,0,0,), 0,0)
            pygame.display.flip()

            nbAttent -= 1

        return True


    def attent(self):
        """Le client attent que la partie commence"""
        # Initialization
        screen = Config.init_screen()
        clock = pygame.time.Clock()
        font = pygame.font.Font(None, 40)
        self.text = "En attente d'autres joueurs"
        background_image, background_rect = Config.load_png(os.path.join(DOSSIER_PARENT, 'sprites/background.png'))

        #On dit au server qu'on est ready !
        connection.Send({"action":"ready", "map":self.propose_map})
        print "Ready !"

        # MAIN LOOP
        while not self.run:
            clock.tick(60) # max speed is 60 frames per second
            connection.Pump()
            self.Pump()
            # Events handling
            for event in pygame.event.get():
               if event.type == pygame.QUIT:
                    return False # closing the window exits the program

            touches=pygame.key.get_pressed()
            if(touches[K_q]):
                return False # exit the program

            screen.blit(background_image, background_rect)
            Config.print_text(font, self.text, screen, (210,255,200))
            pygame.display.flip()

        return True

    def main_loop(self):
        """Main function of the game"""
        encore = True
        while encore:
            encore = self.attent()
            if encore:
                encore = self.partie()

def main_prog():
    try:
        address = sys.argv[1]
        port = sys.argv[2]

        try:
            map = sys.argv[3]
        except IndexError:
            map = None

        game_client = Client(address, int(port), map)
        game_client.main_loop()
    except IndexError:
        print "Using client.py address port [propose_map]"

if __name__ == "__main__":
    main_prog()
    sys.exit()