import os, sys
DOSSIER_COURRANT = os.path.dirname(os.path.abspath(__file__))
DOSSIER_PARENT = os.path.dirname(DOSSIER_COURRANT)
sys.path.append(DOSSIER_PARENT)
import pygame
from pygame.locals import *
from config.config import Config

class Blocks(pygame.sprite.Sprite):
    def __init__(self,cordx,cordy):
        pygame.sprite.Sprite.__init__(self)
        self.image, self.rect = Config.load_png(os.path.join(DOSSIER_PARENT, 'sprites/rocher(petit).png'))
        self.rect.x = cordx*15
        self.rect.y = cordy*15

class Map(pygame.sprite.RenderClear):
    def __init__(self, num):
        print "Map : " + str(num)
        pygame.sprite.RenderClear.__init__(self)
        source = os.path.join(DOSSIER_PARENT,"map/map"+str(num)+".dat")
        fichier = open(source, "r")
        li = []
        self.joueur = []
        while 1:
            ln = fichier.readline().split()
            if ln:
                li.append(ln)
            else:
                break
        fichier.close()
        #ici il faut aller chercher une map suivant sont niveau,
        #charger son contenu et construire les block en fonction de la carte
        for x in range(40):
            for y in range(40):
                if li[x][y] == '1' :
                    self.add(Blocks(y,x))
                if li[x][y] == '2' :
                    self.joueur.append((y*15,x*15))

    @staticmethod
    def get_nb_maps():
        return os.listdir(os.path.join(DOSSIER_PARENT, "map")).__len__()

    def pos_joueurs(self):
        return self.joueur